package com.ssm.gbq.controller;


import com.ssm.gbq.model.Computer;
import com.ssm.gbq.service.ComputerService;
import gbq.ssm.utils.PageBounds;
import gbq.ssm.utils.QiniuCloudUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;


import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.*;

@Controller
@RequestMapping("/computer")
public class ComputerController {
    @Autowired
    private ComputerService computerService;

    @ResponseBody
    @RequestMapping(value = "/openComputerTable", method = RequestMethod.POST)
    public HashMap<String, Object> openComputerTable(Computer computer, int pageSize, int currentPage) throws Exception {
        HashMap<String, Object> result = new HashMap<String, Object>();
        PageBounds<Computer> pageBounds = computerService.openComputerTable(computer, currentPage, pageSize);
        PageBounds<Computer> list = computerService.selectByName(computer);
        if (computer.getDname() != null && computer.getDname() != "" || computer.getCreateTime() != null && computer.getShellTime() != null
        ) {
            result.put("data", list);
        } else {
            result.put("data", pageBounds);
        }
        return result;
    }
    /**
     *
     * 加载电脑类别信息
     */
    @ResponseBody
    @RequestMapping(value = "/getAllComputerType", method = RequestMethod.POST)
    public HashMap<String, Object> getAllComputerType(Computer computer){
        HashMap<String,Object> result = new HashMap<String,Object>();
        List<Computer> list =computerService.getAllComputerType(computer);
        result.put("data",list);
        return result;
    }

    /**
     * 添加
     */
    @ResponseBody
    @RequestMapping(value = "/addComputer", method = RequestMethod.POST)
    public HashMap<String, Object> addComputer(Computer computer) {
        HashMap<String, Object> result = new HashMap<String, Object>();
        Date date = new Date();
        computer.setCreateTime(date);
        computerService.addComputer(computer);
        return result;
    }

    /**
     * 修改
     */
    @ResponseBody
    @RequestMapping(value = "/updateComputer", method = RequestMethod.POST)
    public HashMap<String, Object> updateComputer(Computer computer) {
        HashMap<String, Object> result = new HashMap<String, Object>();
        computerService.updateComputer(computer);
        return result;
    }

    /**
     * 上传图片
     */
    @ResponseBody
    @RequestMapping(value = "/uploadImg", method = RequestMethod.POST)
    public HashMap<String, Object> uploadImg(@RequestParam MultipartFile name, HttpServletRequest request) {
        HashMap<String, Object> result = new HashMap<String, Object>();
        if (name.isEmpty()) {
            result.put("status", 400);
            result.put("Msg", "文件为空，请重新上传");
            return result;
        }
        try {
            byte[] bytes = name.getBytes();
            String imageName = UUID.randomUUID().toString();
            try {
                //使用base64方式上传到七牛云
                String url = QiniuCloudUtil.put64image(bytes, imageName);
                String urlPicture=QiniuCloudUtil.getDOMAIN()+"/"+imageName;
                result.put("status", 200);
                result.put("Msg", "文件上传成功");
                result.put("url", urlPicture);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            result.put("status", 500);
            result.put("Msg", "文件上传发生异常！");
        } finally {
            return result;
        }
    }

    /**
     * 通过id删除
     *
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/deleteComputerById", method = RequestMethod.POST)
    public HashMap<String, Object> deleteComputerById(Integer id) {
        HashMap<String, Object> result = new HashMap<String, Object>();
        computerService.deleteComputerById(id);
        return result;
    }

    /**
     * 批量删除
     */
    @ResponseBody
    @RequestMapping(value = "/deleteComputerByIds", method = RequestMethod.POST)
    public HashMap<String, Object> deleteComputerByIds(Integer[] ids) {
        HashMap<String, Object> result = new HashMap<String, Object>();
        computerService.deleteComputerByIds(Arrays.asList(ids));
        return result;
    }

}
