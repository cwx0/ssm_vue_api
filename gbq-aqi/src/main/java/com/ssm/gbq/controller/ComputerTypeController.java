package com.ssm.gbq.controller;


import com.ssm.gbq.model.ComputerType;
import com.ssm.gbq.service.ComputerTypeService;
import gbq.ssm.utils.PageBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.HashMap;

@Controller
@RequestMapping("/computerType")
public class ComputerTypeController {

    @Autowired
    private ComputerTypeService computerTypeService;

    @ResponseBody
    @RequestMapping(value = "/openComputerTypeTable", method = RequestMethod.POST)
    public HashMap<String, Object> openComputerTypeTable(ComputerType computerType, int pageSize, int currentPage) throws Exception {
        HashMap<String, Object> result = new HashMap<String, Object>();
        PageBounds<ComputerType> pageBounds = computerTypeService.openComputerTypeTable(computerType, currentPage, pageSize);
        PageBounds<ComputerType> list = computerTypeService.selectByName(computerType);
        if (computerType.getName() != null && computerType.getName() != "") {
            result.put("data", list);
        } else {
            result.put("data", pageBounds);
        }
        return result;
    }

    /**
     * 添加
     */
    @ResponseBody
    @RequestMapping(value = "/addComputerType", method = RequestMethod.POST)
    public HashMap<String, Object> addComputerType(ComputerType computerType) throws Exception {
        HashMap<String, Object> result = new HashMap<String, Object>();
        computerTypeService.addComputerType(computerType);
        return result;
    }

    /**
     * 修改
     */
    @ResponseBody
    @RequestMapping(value = "/updateComputerType", method = RequestMethod.POST)
    public HashMap<String, Object> updateComputerType(ComputerType computerType) {
        HashMap<String, Object> result = new HashMap<String, Object>();
        computerTypeService.updateComputerType(computerType);
        return result;
    }

    /**
     * 删除判断
     **/
    @ResponseBody
    @RequestMapping(value = "/isDelete", method = RequestMethod.POST)
    public HashMap<String, Object> isDelete(Integer[] id) {
        HashMap<String, Object> result = new HashMap<String, Object>();
        computerTypeService.isDelete(Arrays.asList(id));
        return result;
    }

    /**
     * 通过id删除
     *
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/deleteComputerTypeById", method = RequestMethod.POST)
    public HashMap<String, Object> deleteComputerTypeById(Integer id) {
        HashMap<String, Object> result = new HashMap<String, Object>();
        computerTypeService.deleteComputerTypeById(id);
        return result;
    }

    /**
     * 批量删除
     */
    @ResponseBody
    @RequestMapping(value = "/deleteComputerTypeByIds", method = RequestMethod.POST)
    public HashMap<String, Object> deleteComputerTypeByIds(Integer[] ids) {
        HashMap<String, Object> result = new HashMap<String, Object>();
        computerTypeService.deleteComputerTypeByIds(Arrays.asList(ids));
        return result;
    }

}
