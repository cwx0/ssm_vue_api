package com.ssm.gbq.controller;

import com.ssm.gbq.model.Computer;
import com.ssm.gbq.service.ComputerService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = {"classpath:applicationContext.xml", "classpath:springmvc-servlet.xml", "classpath:applicationContext-shiro.xml", "classpath:mybatis-config.xml"})
@WebAppConfiguration
public class ComputerControllerTest {
    @Resource
    private ComputerService computerService;
/*

    @Test
    public void selectAllComputer(Computer computer) {
        List<Computer> list = computerService.selectAllComputer(computer);
        for (Computer computer1 : list) {
            System.out.println(computer1);
        }
    }
*/

   /*  @Test
     public void openComputerTable(Computer computer, int pageSize, int currentPage) {
         List<Computer> computers = null;
         try {
             computers = (List<Computer>) computerService.openComputerTable(computer, currentPage, pageSize);
         } catch (Exception e) {
             e.printStackTrace();
         }
         for (Computer computer1 : computers) {
             System.out.println(computer1);
         }
     }
*/
    @Test
    public void addComputer() throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat();
        Computer computer = new Computer();
        computer.setPid(12);
        computer.setDname("123");
        computer.setColor("er");
        computer.setCreateTime(format.parse("2018/9/8"));
        computer.setShellTime(format.parse("2019/5/5"));
        computer.setPicture("23");
        computerService.addComputer(computer);
    }

    @Test
    public void updateComputer() {
        Computer computer = new Computer();
        computer.setId(31);
        computer.setDname("4444");
        computerService.updateComputer(computer);
    }

    @Test
    public void deleteComputerById() {
        computerService.deleteComputerById(44);

    }
}