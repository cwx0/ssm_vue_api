package com.ssm.gbq.dao;

import com.ssm.gbq.model.Computer;
import gbq.ssm.utils.PageBounds;

import java.util.List;


public interface ComputerDao {

    /**
     * 获取列表
     */
    PageBounds<Computer> openComputerTable(Computer computer, int currentPage, int pageSize) throws Exception;

    /**
     * 加载类别
     */
    List<Computer> getAllComputerType(Computer computer);

    /**
     * 多条件查询
     */
    PageBounds<Computer> selectByName(Computer computer);

    /**
     * 删除信息
     */
    int deleteComputerById(Integer id);


    /**
     * 批量删除
     */
    void deleteComputerByIds(List<Integer> ids);

    /**
     * 添加
     */
    int addComputer(Computer computer) throws Exception;

    /**
     * 修改
     */
    int updateComputer(Computer computer);

    /**
     * 根据ID查询
     */
    Computer findById(Integer id);


}
