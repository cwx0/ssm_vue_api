package com.ssm.gbq.dao.impl;

import com.ssm.gbq.dao.ComputerDao;
import com.ssm.gbq.mapper.ComputerMapper;
import com.ssm.gbq.model.Computer;
import gbq.ssm.utils.PageBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ComputerDaoImpl implements ComputerDao {

    @Autowired
    private ComputerMapper computerMapper;

    @Override
    public PageBounds<Computer> openComputerTable(Computer computer, int currentPage, int pageSize) throws Exception {
        final int totalSize = computerMapper.countByExample(computer);
        PageBounds<Computer> pageBounds = new PageBounds<Computer>(currentPage, totalSize, pageSize);
        List<Computer> list = computerMapper.selectByLimitPage(computer, pageBounds.getOffset(), pageBounds.getPageSize());
        pageBounds.setPageList(list);
        return pageBounds;
    }

    @Override
    public List<Computer> getAllComputerType(Computer computer) {
        return computerMapper.getAllComputerType(computer);
    }

    @Override
    public PageBounds<Computer> selectByName(Computer computer) {
        final int totalSize = computerMapper.countByExample(computer);
        PageBounds<Computer> pageBounds = new PageBounds<Computer>(computer);
        List<Computer> list = computerMapper.selectByName(computer, totalSize);
        pageBounds.setPageList(list);
        return pageBounds;
    }

    @Override
    public int deleteComputerById(Integer id) {
        return computerMapper.deleteComputerById(id);
    }


    @Override
    public void deleteComputerByIds(List<Integer> ids) {
        computerMapper.deleteComputerByIds(ids);
    }

    @Override
    public int addComputer(Computer computer) throws Exception {
        return computerMapper.addComputer(computer);
    }

    @Override
    public int updateComputer(Computer computer) {
        return computerMapper.updateComputer(computer);
    }

    @Override
    public Computer findById(Integer id) {
        List<Computer> list = (List<Computer>) computerMapper.findById(id);
        return (Computer) list;
    }
}
