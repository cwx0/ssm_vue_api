package com.ssm.gbq.dao.impl;

import com.ssm.gbq.dao.ComputerTypeDao;
import com.ssm.gbq.mapper.ComputerTypeMapper;
import com.ssm.gbq.model.ComputerType;
import gbq.ssm.utils.PageBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.List;

@Repository
public class ComputerTypeDaoImpl implements ComputerTypeDao {

    @Autowired
    private ComputerTypeMapper compputerTypeMapper;

    @Override
    public PageBounds<ComputerType> openComputerTypeTable(ComputerType computerType, int currentPage, int pageSize) throws Exception {
        final int totalSize = compputerTypeMapper.countByExample(computerType);
        PageBounds<ComputerType> pageBounds = new PageBounds<ComputerType>(currentPage, totalSize, pageSize);
        List<ComputerType> list = compputerTypeMapper.selectByLimitPage(computerType, pageBounds.getOffset(), pageBounds.getPageSize());
        pageBounds.setPageList(list);
        return pageBounds;
    }

    @Override
    public PageBounds<ComputerType> selectByName(ComputerType computerType) {
        final int totalSize = compputerTypeMapper.countByExample(computerType);
        PageBounds<ComputerType> pageBounds = new PageBounds<ComputerType>(computerType);
        List<ComputerType> list = compputerTypeMapper.selectByName(computerType);
        pageBounds.setPageList(list);
        return pageBounds;
    }

    @Override
    public void isDelete(List<Integer> id) {
         compputerTypeMapper.isDelete(id);
    }


    @Override
    public int deleteComputerTypeById(Integer id) {
        return compputerTypeMapper.deleteComputerTypeById(id);
    }

    @Override
    public void deleteComputerTypeByIds(List<Integer> ids) {
        compputerTypeMapper.deleteComputerTypeByIds(ids);
    }

    @Override
    public int addComputerType(ComputerType computerType) throws Exception {
        return compputerTypeMapper.addComputerType(computerType);
    }

    @Override
    public int updateComputerType(ComputerType computerType) {
        return compputerTypeMapper.updateComputerType(computerType);
    }

    @Override
    public ComputerType findById(Integer id) {
        return compputerTypeMapper.findById(id);
    }
}
