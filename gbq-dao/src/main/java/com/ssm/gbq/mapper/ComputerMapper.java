package com.ssm.gbq.mapper;

import com.ssm.gbq.model.Computer;
import gbq.ssm.utils.PageBounds;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ComputerMapper {

    /**
     *
     */
    int countByExample(@Param("computer") Computer computer);

    /**
     *
     */
    List<Computer> selectByLimitPage(@Param("computer") Computer computer, @Param("offset") int offset, @Param("limit") int pageSize);

    /**
     * 获取列表
     */
    PageBounds<Computer> openComputerTable(@Param("computer")Computer computer, int currentPage, int pageSize) throws Exception;

    /**
     * 加载类别
     */
    List<Computer> getAllComputerType(@Param("computer")Computer computer);

    /**
     * 多条件查询
     */
    List<Computer> selectByName(@Param("computer")Computer computer, int currentPage);

    /**
     * 删除信息
     */
    int deleteComputerById(Integer id);

    /**
     * 批量删除
     */
     void deleteComputerByIds(List<Integer> ids);
    /**
     * 添加
     */
     int addComputer(Computer computer);

    /**
     * 修改
     */
     int updateComputer(Computer computer);

    /**
     * 根据ID查询
     */
     Computer findById(Integer id);

}
