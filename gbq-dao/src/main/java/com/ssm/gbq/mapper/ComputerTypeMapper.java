package com.ssm.gbq.mapper;

import com.ssm.gbq.model.ComputerType;
import gbq.ssm.utils.PageBounds;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ComputerTypeMapper {

    /**
     *
     */
    int countByExample(@Param("computerType") ComputerType computerType);

    /**
     *
     */
    List<ComputerType> selectByLimitPage(@Param("computerType") ComputerType computerType, @Param("offset") int offset, @Param("limit") int pageSize);

    /**
     * 获取列表
     */
    PageBounds<ComputerType> openComputerTypeTable(@Param("computerType") ComputerType computerType, int currentPage, int pageSize) throws Exception;

    /**
     * 多条件查询
     */
    List<ComputerType> selectByName(@Param("computerType") ComputerType computerType);

    /**
     * 判断删除信息
     */
    void isDelete(List<Integer> id);

    /**
     * 删除信息
     */
    int deleteComputerTypeById(Integer id);

    /**
     * 批量删除
     */
    void deleteComputerTypeByIds(List<Integer> ids);

    /**
     * 添加
     */
    int addComputerType(ComputerType computerType);

    /**
     * 修改
     */
    int updateComputerType(ComputerType computerType);

    /**
     * 根据ID查询
     */
    ComputerType findById(Integer id);

}
