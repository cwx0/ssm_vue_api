package com.ssm.gbq.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class Computer {
    private Integer id;
    private Integer pid;
    private String dname;
    private String color;
    @DateTimeFormat(pattern = "yyyy-MM-dd")//从页面传后台做转换
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")//后台传页面做转换
    private Date createTime;
    @DateTimeFormat(pattern = "yyyy-MM-dd")//从页面传后台做转换
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")//后台传页面做转换
    private Date shellTime;
    private String picture;

    private ComputerType computerType;

    public ComputerType getComputerType() {
        return computerType;
    }

    public void setComputerType(ComputerType computerType) {
        this.computerType = computerType;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public String getDname() {
        return dname;
    }

    public void setDname(String dname) {
        this.dname = dname;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getShellTime() {
        return shellTime;
    }

    public void setShellTime(Date shellTime) {
        this.shellTime = shellTime;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    @Override
    public String toString() {
        return "Computer{" +
                "id=" + id +
                ", pid=" + pid +
                ", dname='" + dname + '\'' +
                ", color='" + color + '\'' +
                ", createTime=" + createTime +
                ", shellTime=" + shellTime +
                ", picture='" + picture + '\'' +
                '}';
    }
}
