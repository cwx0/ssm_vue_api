package com.ssm.gbq.service;

import com.ssm.gbq.model.ComputerType;
import gbq.ssm.utils.PageBounds;

import java.util.List;

public interface ComputerTypeService {

    /**
     * 获取列表
     */
    PageBounds<ComputerType> openComputerTypeTable(ComputerType computerType, int currentPage, int pageSize) throws Exception;

    /**
     * 多条件查询
     */
    PageBounds<ComputerType> selectByName(ComputerType computerType);

    /**
     * 删除判断信息
     */
    void isDelete(List<Integer> id);

    /**
     * 删除信息
     */
    int deleteComputerTypeById(Integer id);

    /**
     * 批量删除
     */
    void deleteComputerTypeByIds(List<Integer> ids);

    /**
     * 添加
     */
    int addComputerType(ComputerType computerType) throws Exception;

    /**
     * 修改
     */
    int updateComputerType(ComputerType computerType);

    /**
     * 根据ID查询
     */
    ComputerType findById(Integer id);
}
