package com.ssm.gbq.service.impl;

import com.ssm.gbq.dao.ComputerDao;
import com.ssm.gbq.model.Computer;
import com.ssm.gbq.service.ComputerService;
import gbq.ssm.utils.BusinessException;
import gbq.ssm.utils.PageBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ComputerServiceImpl implements ComputerService {

    @Autowired
    private ComputerDao computerDao;

    @Override
    public PageBounds<Computer> openComputerTable(Computer computer, int currentPage, int pageSize) {
        try {
            return computerDao.openComputerTable(computer, currentPage, pageSize);
        } catch (Exception e) {
            throw new BusinessException("获取列表失败！", e.getMessage());
        }
    }

    @Override
    public List<Computer> getAllComputerType(Computer computer) {
        return computerDao.getAllComputerType(computer);
    }

    @Override
   public PageBounds<Computer> selectByName(Computer computer) throws BusinessException {
       return computerDao.selectByName(computer);
   }

    @Override
    public int deleteComputerById(Integer id) {

        return computerDao.deleteComputerById(id);
    }

    @Override
    public void deleteComputerByIds(List<Integer> ids) {
       computerDao.deleteComputerByIds(ids);
    }

    @Override
    public int addComputer(Computer computer) throws BusinessException {
        try {
            return computerDao.addComputer(computer);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public int updateComputer(Computer computer) {
        return computerDao.updateComputer(computer);
    }

    @Override
    public Computer findById(Integer id) {
        return computerDao.findById(id);
    }
}
