package com.ssm.gbq.service.impl;

import com.ssm.gbq.dao.ComputerTypeDao;
import com.ssm.gbq.model.ComputerType;
import com.ssm.gbq.service.ComputerTypeService;
import gbq.ssm.utils.BusinessException;
import gbq.ssm.utils.PageBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ComputerTypeServiceImpl implements ComputerTypeService{

    @Autowired
    private ComputerTypeDao computerTypeDao;

    @Override
    public PageBounds<ComputerType> openComputerTypeTable(ComputerType computerType, int currentPage, int pageSize) throws Exception {
        try {
            return computerTypeDao.openComputerTypeTable(computerType, currentPage, pageSize);
        } catch (Exception e) {
            throw new BusinessException("获取列表失败！", e.getMessage());
        }
    }

    @Override
    public PageBounds<ComputerType> selectByName(ComputerType computerType) {
        return computerTypeDao.selectByName(computerType);
    }

    @Override
    public void isDelete(List<Integer> id) {
         computerTypeDao.isDelete(id);
    }

    @Override
    public int deleteComputerTypeById(Integer id) {
        return computerTypeDao.deleteComputerTypeById(id);
    }

    @Override
    public void deleteComputerTypeByIds(List<Integer> ids) {
        computerTypeDao.deleteComputerTypeByIds(ids);
    }

    @Override
    public int addComputerType(ComputerType computerType) throws Exception {
        return computerTypeDao.addComputerType(computerType);
    }

    @Override
    public int updateComputerType(ComputerType computerType) {
        return computerTypeDao.updateComputerType(computerType);
    }

    @Override
    public ComputerType findById(Integer id) {
        return computerTypeDao.findById(id);
    }
}
